package model;

public class Player
{
	private static final String DEFAULT_NAME = "John Cena";
	private String myName;
	private int myNumberWins;
	private boolean myAmAI;
	private Hand myHand;

	public Player(String name)
	{
		myName = name;
		myHand = new Hand(5);
	}

	public boolean validateName(String name)
	{
		return false;
	}

	public int incrementNumberWins()
	{
		return 0;
	}

	public String toString()
	{
		return null;
	}

	public Object clone()
	{
		return null;
	}

	public Hand getHand()
	{
		return null;
	}

	public String getName()
	{
		return null;
	}

	public int getNumberWins()
	{
		return 0;
	}

	public boolean getAmAI()
	{	
		return false;
	}

}
