package modeltest;

import static org.junit.Assert.*;

import java.awt.Image;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardSuit;
import model.CardType;
import model.PokerHand;

public class pokerHandTest 
{
	private PokerHand myPokerHand;
	private int myMaxNumberCards = 5;
	private Vector<Card> myCards;
	private Card myCard;
	private CardType myType;
	private CardSuit mySuit;
	private Image myImage;
	

	@Before
	public void setup()
	{
		myPokerHand = new PokerHand(myMaxNumberCards);

	}

	@Test
	public void testHighCard()
	{
		myCard = new Card(mySuit.DIAMONDS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.NINE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.SEVEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.THREE, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be high card", myPokerHand.isHighCard());
	}
	
	@Test
	public void testDetermineRanking1()
	{
		testHighCard();
		
		assertTrue("Should rank as 1", myPokerHand.determineRanking() == 1);
	}
	
	@Test
	public void testGetRanking1()
	{
		testHighCard();
		
		assertTrue("Should rank as 1", myPokerHand.getRanking() == 1);
	}
	
	@Test
	public void testPair()
	{
		myCard = new Card(mySuit.DIAMONDS, myType.TWO, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.NINE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.THREE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.THREE, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be pair", myPokerHand.isPair());
	}
	
	@Test
	public void testDetermineRanking2()
	{
		testPair();
		
		assertTrue("Should rank as 2", myPokerHand.determineRanking() == 2);
	}
	
	@Test
	public void testGetRanking2()
	{
		testPair();
		
		assertTrue("Should rank as 2", myPokerHand.getRanking() == 2);
	}
	
	@Test
	public void testTwoPair()
	{
		myCard = new Card(mySuit.DIAMONDS, myType.TWO, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.THREE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.THREE, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be two pair", myPokerHand.isTwoPair());
	}
	
	@Test
	public void testDetermineRanking3()
	{
		testTwoPair();
		
		assertTrue("Should rank as 3", myPokerHand.determineRanking() == 3);
	}
	
	@Test
	public void testGetRanking3()
	{
		testTwoPair();
		
		assertTrue("Should rank as 3", myPokerHand.getRanking() == 3);
	}
	
	@Test
	public void testThreeOfKind()
	{
		myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.TEN, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be three of kind", myPokerHand.isThreeOfKind());
	}
	
	@Test
	public void testDetermineRanking4()
	{
		testThreeOfKind();
		
		assertTrue("Should rank as 4", myPokerHand.determineRanking() == 4);
	}
	
	@Test
	public void testGetRanking4()
	{
		testThreeOfKind();
		
		assertTrue("Should rank as 4", myPokerHand.getRanking() == 4);
	}
	
	@Test
	public void testStraight1()
	{
		myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.TEN, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be Straight", myPokerHand.isStraight());
	}
	
	@Test
	public void testDetermineRanking5()
	{
		testStraight1();
		
		assertTrue("Should rank as 5", myPokerHand.determineRanking() == 5);
	}
	
	@Test
	public void testGetRanking5()
	{
		testStraight1();
		
		assertTrue("Should rank as 5", myPokerHand.getRanking() == 5);
	}
	
	@Test
	public void testStraight2()
	{
		myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.TWO, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.THREE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be Straight", myPokerHand.isStraight());
	}
	
	@Test
	public void testDetermineRanking6()
	{
		testStraight2();
		
		assertTrue("Should rank as 5", myPokerHand.determineRanking() == 5);
	}
	
	@Test
	public void testGetRanking6()
	{
		testStraight2();
		
		assertTrue("Should rank as 5", myPokerHand.getRanking() == 5);
	}
	
	@Test
	public void testIsFlush1() 
	{
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.NINE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.SIX, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be flush", myPokerHand.isFlush());
	}
	
	@Test
	public void testDetermineRanking7()
	{
		testIsFlush1();
		
		assertTrue("Should rank as 6", myPokerHand.determineRanking() == 6);
	}
	
	@Test
	public void testGetRanking7()
	{
		testIsFlush1();
		
		assertTrue("Should rank as 6", myPokerHand.getRanking() == 6);
	}
	
	@Test
	public void testIsFlush2() 
	{
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.NINE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.SIX, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should be flush", myPokerHand.isFlush());
		assertTrue("Should be high card", myPokerHand.isHighCard());
	}
	
	@Test
	public void testDetermineRanking8()
	{
		testIsFlush2();
		
		assertTrue("Should rank as 1", myPokerHand.determineRanking() == 1);
	}
	
	@Test
	public void testGetRanking8()
	{
		testIsFlush2();
		
		assertTrue("Should rank as 1", myPokerHand.getRanking() == 1);
	}
	
	@Test
	public void testIsFullHouse1() 
	{
		myCard = new Card(mySuit.CLUBS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.SIX, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.SIX, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be Full House", myPokerHand.isFullHouse());
	}
	
	@Test
	public void testDetermineRanking9()
	{
		testIsFullHouse1();
		
		assertTrue("Should rank as 7", myPokerHand.determineRanking() == 7);
	}
	
	@Test
	public void testGetRanking9()
	{
		testIsFullHouse1();
		
		assertTrue("Should rank as 7", myPokerHand.getRanking() == 7);
	}
	
	@Test
	public void testIsFullHouse2() 
	{
		myCard = new Card(mySuit.CLUBS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.SIX, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.SEVEN, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should  not be Full House", myPokerHand.isFullHouse());
		assertTrue("Should be three of kind", myPokerHand.isThreeOfKind());
	}
	
	@Test
	public void testDetermineRanking10()
	{
		testIsFullHouse2();
		
		assertTrue("Should rank as 4", myPokerHand.determineRanking() == 4);
	}
	
	@Test
	public void testGetRanking10()
	{
		testIsFullHouse2();
		
		assertTrue("Should rank as 4", myPokerHand.getRanking() == 4);
	}
	
	@Test
	public void testIsFullHouse3() 
	{
		myCard = new Card(mySuit.CLUBS, myType.SEVEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.FOUR, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.SIX, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.SIX, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should  not be Full House", myPokerHand.isFullHouse());
		assertTrue("Should be two pair", myPokerHand.isTwoPair());
	}
	
	@Test
	public void testDetermineRanking11()
	{
		testIsFullHouse3();
		
		assertTrue("Should rank as 3", myPokerHand.determineRanking() == 3);
	}
	
	@Test
	public void testGetRanking11()
	{
		testIsFullHouse3();
		
		assertTrue("Should rank as 3", myPokerHand.getRanking() == 3);
	}
	
	@Test
	public void testIsFourOfKing1() 
	{
		myCard = new Card(mySuit.CLUBS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.EIGHT, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be four of kind", myPokerHand.isFourOfKind());
	}
	
	@Test
	public void testDetermineRanking12()
	{
		testIsFourOfKing1();
		
		assertTrue("Should rank as 8", myPokerHand.determineRanking() == 8);
	}
	
	@Test
	public void testGetRanking12()
	{
		testIsFourOfKing1();
		
		assertTrue("Should rank as 8", myPokerHand.getRanking() == 8);
	}
	
	@Test
	public void testIsFourOfKing2() 
	{
		myCard = new Card(mySuit.CLUBS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.EIGHT, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.EIGHT, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should not be four of kind", myPokerHand.isFourOfKind());
		assertTrue("Should be full house", myPokerHand.isFullHouse());
	}
	
	@Test
	public void testDetermineRanking13()
	{
		testIsFourOfKing2();
		
		assertTrue("Should rank as 7", myPokerHand.determineRanking() == 7);
	}
	
	@Test
	public void testGetRanking13()
	{
		testIsFourOfKing2();
		
		assertTrue("Should rank as 7", myPokerHand.getRanking() == 7);
	}
	
	@Test
	public void testIsFourOfKing3() 
	{
		myCard = new Card(mySuit.CLUBS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.HEARTS, myType.FIVE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.SPADES, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.EIGHT, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should not be four of kind", myPokerHand.isFourOfKind());
		assertTrue("Should be three of kind", myPokerHand.isThreeOfKind());
	}
	
	@Test
	public void testDetermineRanking14()
	{
		testIsFourOfKing3();
		
		assertTrue("Should rank as 4", myPokerHand.determineRanking() == 4);
	}
	
	@Test
	public void testGetRanking14()
	{
		testIsFourOfKing3();
		
		assertTrue("Should rank as 4", myPokerHand.getRanking() == 4);
	}
	
	@Test
	public void testIsStraightFlush1() 
	{
		myCard = new Card(mySuit.DIAMONDS, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.TEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.NINE, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be straignt flush", myPokerHand.isStraightFlush());
	}
	
	@Test
	public void testDetermineRanking15()
	{
		testIsStraightFlush1();
		
		assertTrue("Should rank as 9", myPokerHand.determineRanking() == 9);
	}
	
	@Test
	public void testGetRanking15()
	{
		testIsStraightFlush1();
		
		assertTrue("Should rank as 9", myPokerHand.getRanking() == 9);
	}
	
	@Test
	public void testIsStraightFlush2() 
	{
		myCard = new Card(mySuit.HEARTS, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.TEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.NINE, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should not be Straight flush", myPokerHand.isStraightFlush());
		assertTrue("Should be Straight", myPokerHand.isStraight());
	}
	
	@Test
	public void testDetermineRanking16()
	{
		testIsStraightFlush2();
		
		assertTrue("Should rank as 5", myPokerHand.determineRanking() == 5);
	}
	
	@Test
	public void testGetRanking16()
	{
		testIsStraightFlush2();
		
		assertTrue("Should rank as 5", myPokerHand.getRanking() == 5);
	}
	
	@Test
	public void testIsStraightFlush3() 
	{
		myCard = new Card(mySuit.DIAMONDS, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.TEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.DIAMONDS, myType.SIX, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should be straignt flush", myPokerHand.isStraightFlush());
		assertTrue("Should be flush", myPokerHand.isFlush());
	}
	
	@Test
	public void testDetermineRanking17()
	{
		testIsStraightFlush3();
		
		assertTrue("Should rank as 6", myPokerHand.determineRanking() == 6);
	}
	
	@Test
	public void testGetRanking17()
	{
		testIsStraightFlush3();
		
		assertTrue("Should rank as 6", myPokerHand.getRanking() == 6);
	}
	
	@Test
	public void testIsRoyalFlush1() 
	{
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.TEN, myImage);
		myPokerHand.add(myCard);
		
		assertTrue("Should be royal flush", myPokerHand.isRoyalFlush());
	}
	
	@Test
	public void testDetermineRanking18()
	{
		testIsRoyalFlush1();
		
		assertTrue("Should rank as 10", myPokerHand.determineRanking() == 10);
	}
	
	@Test
	public void testGetRanking18()
	{
		testIsRoyalFlush1();
		
		assertTrue("Should rank as 10", myPokerHand.getRanking() == 10);
	}
	
	@Test
	public void testIsRoyalFlush2() 
	{
		myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.TEN, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should not be royal flush", myPokerHand.isRoyalFlush());
		assertTrue("Should be Straight", myPokerHand.isStraight());
	}
	
	@Test
	public void testDetermineRanking19()
	{
		testIsRoyalFlush2();
		
		assertTrue("Should rank as 5", myPokerHand.determineRanking() == 5);
	}
	
	@Test
	public void testGetRanking19()
	{
		testIsRoyalFlush2();
		
		assertTrue("Should rank as 5", myPokerHand.getRanking() == 5);
	}
	
	@Test
	public void testIsRoyalFlush3() 
	{
		myCard = new Card(mySuit.CLUBS, myType.THREE, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.KING, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.QUEEN, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.JACK, myImage);
		myPokerHand.add(myCard);
		myCard = new Card(mySuit.CLUBS, myType.TEN, myImage);
		myPokerHand.add(myCard);
		
		assertFalse("Should not be royal flush", myPokerHand.isRoyalFlush());
		assertTrue("Should be flush", myPokerHand.isFlush());
	}

	@Test
	public void testDetermineRanking20()
	{
		testIsRoyalFlush3();
		
		assertTrue("Should rank as 6", myPokerHand.determineRanking() == 6);
	}
	
	@Test
	public void testGetRanking20()
	{
		testIsRoyalFlush3();
		
		assertTrue("Should rank as 6", myPokerHand.getRanking() == 6);
	}
}
