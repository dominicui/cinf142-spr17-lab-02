package modeltest;

import static org.junit.Assert.*;

import java.awt.Image;
import java.util.Random;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardSuit;
import model.CardType;
import model.Hand;

public class handTest 
{
	private Hand myHand;
	private Vector<Card> myCards;
	private Vector<Integer> indices; 
	private int myMaxNumberCards = 5;
	private Card myCard;
	private CardType myType;
	private CardSuit mySuit;
	private Image myImage;
	
	@Before
	public void setup()
	{
		myHand = new Hand(myMaxNumberCards);
	}

	@Test
	public void testAdd1() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
	}

	@Test
	public void testAdd2() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
	}
	
	@Test
	public void testAdd3() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
	}
	
	@Test
	public void testAdd4() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
	}
	
	@Test
	public void testAdd5() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myHand.add(myCard);
		assertFalse("Should be a succeed add", myHand.add(myCard));
	}
	
	@Test
	public void testAdd6() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.SPADES, myType.EIGHT, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
	}
	
	@Test
	public void testAdd7() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.SPADES, myType.EIGHT, myImage);
		myHand.add(myCard);
		assertTrue("Should be a succeed add", myHand.add(myCard));
		
		myCard = new Card(mySuit.DIAMONDS, myType.EIGHT, myImage);
		myHand.add(myCard);
		assertFalse("Should be an unsucceed add", myHand.add(myCard));
	}
	
	@Test
	public void testDiscard1()
	{
		handHelper();
		
		indices = new Vector<Integer>();
		indices.add(3);
		
		myHand.discard(indices); 
		assertTrue("Should get rid of card on position 3", myHand.getCards() == myCards);
		
		assertTrue("Should have 4 cards left", myHand.getNumberCardsInHand() == 4);
	}
	
	@Test
	public void testOrderCards1()
	{
		testDiscard1();
		
		myHand.orderCards();
		assertTrue("Should have 5 cards left", myHand.getNumberCardsInHand() == 5);
	}
	
	@Test
	public void testDiscard2()
	{
		handHelper();
		
		indices = new Vector<Integer>();
		indices.add(3);
		indices.add(1);
		
		myHand.discard(indices); 
		assertTrue("Should get rid of card on position 3", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 1", myHand.getCards() == myCards);
		
		assertTrue("Should have 3 cards left", myHand.getNumberCardsInHand() == 3);
	}
	
	@Test
	public void testOrderCards2()
	{
		testDiscard2();
		
		myHand.orderCards();
		assertTrue("Should have 4 cards left", myHand.getNumberCardsInHand() == 4);
		
		myHand.orderCards();
		assertTrue("Should have 5 cards left", myHand.getNumberCardsInHand() == 5);
	}
	
	@Test
	public void testDiscard3()
	{
		handHelper();
		
		indices = new Vector<Integer>();
		indices.add(3);
		indices.add(1);
		indices.add(0);
		
		myHand.discard(indices); 
		assertTrue("Should get rid of card on position 3", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 1", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 0", myHand.getCards() == myCards);
		
		assertTrue("Should have 2 cards left", myHand.getNumberCardsInHand() == 2);
	}
	
	@Test
	public void testOrderCards3()
	{
		testDiscard3();
		
		myHand.orderCards();
		assertTrue("Should have 3 cards left", myHand.getNumberCardsInHand() == 3);
		
		myHand.orderCards();
		assertTrue("Should have 4 cards left", myHand.getNumberCardsInHand() == 4);
		
		myHand.orderCards();
		assertTrue("Should have 5 cards left", myHand.getNumberCardsInHand() == 5);
	}
	
	@Test
	public void testDiscard4()
	{
		handHelper();
		
		indices = new Vector<Integer>();
		indices.add(3);
		indices.add(1);
		indices.add(0);
		indices.add(4);
		
		myHand.discard(indices); 
		assertTrue("Should get rid of card on position 3", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 1", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 0", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 4", myHand.getCards() == myCards);
		
		assertTrue("Should have 1 cards left", myHand.getNumberCardsInHand() == 1);
	}
	
	@Test
	public void testOrderCards4()
	{
		testDiscard4();
		
		myHand.orderCards();
		assertTrue("Should have 2 cards left", myHand.getNumberCardsInHand() == 2);
		
		myHand.orderCards();
		assertTrue("Should have 3 cards left", myHand.getNumberCardsInHand() == 3);
		
		myHand.orderCards();
		assertTrue("Should have 4 cards left", myHand.getNumberCardsInHand() == 4);
		
		myHand.orderCards();
		assertTrue("Should have 5 cards left", myHand.getNumberCardsInHand() == 5);
	}
	
	@Test
	public void testDiscard5()
	{
		handHelper();
		
		indices = new Vector<Integer>();
		indices.add(3);
		indices.add(1);
		indices.add(0);
		indices.add(4);
		indices.add(2);
		
		myHand.discard(indices); 
		assertTrue("Should get rid of card on position 3", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 1", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 0", myHand.getCards() == myCards);
		assertTrue("Should get rid of card on position 4", myHand.getCards() == myCards);
		
		assertTrue("Should have no cards left", myHand.getNumberCardsInHand() == 0);
	}
	
	@Test
	public void testOrderCards5()
	{
		testDiscard4();
		
		myHand.orderCards();
		assertTrue("Should have 1 cards left", myHand.getNumberCardsInHand() == 1);
		
		myHand.orderCards();
		assertTrue("Should have 2 cards left", myHand.getNumberCardsInHand() == 2);
		
		myHand.orderCards();
		assertTrue("Should have 3 cards left", myHand.getNumberCardsInHand() == 3);
		
		myHand.orderCards();
		assertTrue("Should have 4 cards left", myHand.getNumberCardsInHand() == 4);
		
		myHand.orderCards();
		assertTrue("Should have 5 cards left", myHand.getNumberCardsInHand() == 5);
	}
	
	public void handHelper()
	{
		myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		myHand.add(myCard);

		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		myHand.add(myCard);

		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		myHand.add(myCard);

		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		myHand.add(myCard);

		myCard = new Card(mySuit.SPADES, myType.EIGHT, myImage);
		myHand.add(myCard);
	}
}
