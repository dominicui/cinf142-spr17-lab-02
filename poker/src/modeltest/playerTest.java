package modeltest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Player;

public class playerTest 
{

	private Player myPlayer;
	private String myName;
	private String DEFAULT_NAME = "John Cena";
	
	@Before
	public void setup()
	{
		myPlayer = new Player(myName);
	}
	
	@Test
	public void testValidPlayerName1() 
	{
		myName = "conrad";
		
		assertTrue("Should be a valid name", myPlayer.validateName(myName));
	}

	@Test
	public void testValidPlayerName2() 
	{
		myName = "Conrad";
		
		assertTrue("Should be a valid name", myPlayer.validateName(myName));
	}
	
	@Test
	public void testValidPlayerName3() 
	{
		myName = "Conrad hall";
		
		assertFalse("Should not have a speace", myPlayer.validateName(myName));
	}
	
	@Test
	public void testValidPlayerName4() 
	{
		myName = "ConradHall";
		
		assertTrue("Should be a valid name", myPlayer.validateName(myName));
	}
	
	@Test
	public void testValidPlayerName5() 
	{
		myName = "Conrad123";
		
		assertTrue("Should be a valid name", myPlayer.validateName(myName));
	}
	
	@Test
	public void testValidPlayerName6() 
	{
		myName = "ConradHall123";
		
		assertTrue("Should be a valid name", myPlayer.validateName(myName));
	}
	
	@Test
	public void testValidPlayerName7() 
	{
		myName = "Conrad123Hall";
		
		assertTrue("Should be a valid name", myPlayer.validateName(myName));
	}
	
	@Test
	public void testValidPlayerName8() 
	{
		myName = "Conrad@Hall";
		
		assertFalse("Should not have special character", myPlayer.validateName(myName));
	}
	
	@Test
	public void testValidPlayerName9() 
	{
		myName = "CONRAD";
		
		assertTrue("Should not have special character", myPlayer.validateName(myName));
	}
	@Test
	public void testDefaultPlayerName() 
	{
		myName = DEFAULT_NAME;
		
		assertTrue("Should be a valid name", myPlayer.validateName(myName));
	}
	
	@Test
	public void testNumberWin1()
	{
		myPlayer.incrementNumberWins();
		myPlayer.incrementNumberWins();
		
		assertTrue("Should have two wins", myPlayer.getNumberWins() == 2);
	}
	
	@Test
	public void testNumberWin2()
	{
		myPlayer.incrementNumberWins();
		myPlayer.incrementNumberWins();
		
		assertTrue("Should have two wins", myPlayer.getNumberWins() == 2);
		
		myPlayer.incrementNumberWins();
		
		assertTrue("Should have three wins", myPlayer.getNumberWins() == 3);

	}
}
