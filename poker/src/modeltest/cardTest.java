package modeltest;
import static org.junit.Assert.*;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardSuit;
import model.CardType;

public class cardTest 
{
	private Card myCard;
	private CardType myType;
	private CardSuit mySuit;
	private Image myImage;
	
	@Before
	public void setup()
	{
		myCard = new Card(mySuit, myType, myImage);
	}

	@Test
	public void testIsFaceUp() 
	{
		assertFalse("card should be face down", myCard.isFaceUp());
	}
	
	@Test
	public void testFlip()
	{
		assertFalse("card should be face down", myCard.isFaceUp());
		
		myCard.flip();
		assertTrue("card should be face up", myCard.isFaceUp());
	}
	
	@Test
	public void testDoubleFlip()
	{
		assertFalse("card should be face down", myCard.isFaceUp());
		
		myCard.flip();
		myCard.flip();
		
		assertFalse("card should be face down", myCard.isFaceUp());
	}
	
	@Test
	public void testGetType1()
	{
		myType = CardType.FOUR;
		
		assertTrue("card type should be "+ myType.getType(), myCard.getType() == myType.getType());
	}
	
	@Test
	public void testGetType2()
	{
		myType = CardType.ACE;
		myType = CardType.JACK;
		
		assertTrue("card type should be JACK(11)", myCard.getType() == 11);
	}

	@Test
	public void testGetSuit1()
	{
		mySuit = CardSuit.HEARTS;
		
		assertTrue("card suit should be "+ mySuit.getSuit(), myCard.getSuit() == mySuit.getSuit());
	}
	
	@Test
	public void testGetSuit2()
	{
		mySuit = CardSuit.HEARTS;
		mySuit = CardSuit.SPADES;
		
		assertTrue("card suittype should be Spades", myCard.getSuit() == "Spades");
	}
	
	@Test
	public void testSuitAndType()
	{
		mySuit = CardSuit.DIAMONDS;
		myType = CardType.QUEEN;
		
		assertTrue("card suit should be "+ mySuit.getSuit(), myCard.getSuit() == mySuit.getSuit());
		assertTrue("card type should be "+ myType.getType(), myCard.getType() == myType.getType());

	}
}
