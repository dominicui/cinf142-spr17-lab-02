package modeltest;

import static org.junit.Assert.*;

import java.awt.Image;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardSuit;
import model.CardType;
import model.ComputerPlayer;
import model.Deck;
import model.Hand;
import model.Player;
import model.PokerHand;
import model.PokerModel;

public class testModel 
{

	private PokerModel myModel;
	private Player[] myPlayers = new Player[2];
	private Player myPlayer;
	private ComputerPlayer myCP;
	
	@Before
	public void setup()
	{
		myPlayers[0] = myPlayer;
		myPlayers[1] = myCP;
		
		myModel = new PokerModel(myPlayers[0]);
	}
	
	@Test
	public void testSwitchTurns1()
	{
		assertTrue("switch 0 times", myModel.switchTurns() == 0);
	}
	
	@Test
	public void testSwitchTurns2()
	{
		
		myModel.dealCards();
		
		assertTrue("switch 1 times", myModel.switchTurns() == 1);
	}
	
	@Test
	public void testSwitchTurns3()
	{
		myModel.dealCards();
		myModel.dealCards();
		
		assertTrue("switch 2 times", myModel.switchTurns() == 2);
	}
	
	@Test
	public void testSwitchTurns4()
	{
		myModel.dealCards();
		myModel.dealCards();
		myModel.dealCards();
		
		assertTrue("switch 3 times", myModel.switchTurns() == 3);
	}
	
	@Test
	public void testDetermineWinne1r()
	{
		FourOfKind();
		myPlayer.getHand().equals(bigHand);
		ThreeOfKind();
		myCP.getHand().equals(smallHand);
		
		assertTrue("Player win", myModel.determineWinner() == myPlayer);
	}
	
	@Test
	public void testDetermineWinner2()
	{
		
		myPlayer.getHand().equals(smallHand);
		myCP.getHand().equals(bigHand);
		
		assertTrue("Player win", myModel.determineWinner() == myCP);
	}
	
	@Test
	public void testReset1()
	{
		assertFalse("Should not be reseted", myModel.resetGame());
	}
	
	@Test
	public void testReset2()
	{
		newDeck();
		
		assertTrue
		("Should not be reseted", myModel.resetGame());
	}
	
	@Test
	public void testGetPlayer1() 
	{
		assertTrue("Shloud be user Player", myModel.getPlayer(0) == myPlayer);
	}

	@Test
	public void testGetPlayer2() 
	{
		assertTrue("Shloud be user Player", myModel.getPlayer(1) == myCP);
	}
	
	@Test
	public void testGetPlayerUp1()
	{
		myModel.dealCards();
		
		assertTrue("computer player's turn", myModel.getPlayerUp() == myCP);
		assertTrue("index of players should be 0", myModel.getIndexPlayerUp() == 1);
	}
	
	@Test
	public void testGetPlayerUp2()
	{
		myModel.dealCards();
		myModel.dealCards();
		
		assertTrue("user player's turn", myModel.getPlayerUp() == myPlayer);
		assertTrue("index of players should be 0", myModel.getIndexPlayerUp() == 0);
	}
	
	/**
	 * two helper function that help determine winner.
	 *
	 * @Dominic
	 */
	
	private Hand smallHand = new Hand(5);
	private Hand bigHand = new Hand(5);
	private Card myCard;
	private CardType myType;
	private CardSuit mySuit;
	private Image myImage;
	
	public Hand FourOfKind() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		bigHand.add(myCard);

		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		bigHand.add(myCard);
		
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		bigHand.add(myCard);
		
		myCard = new Card(mySuit.SPADES, myType.ACE, myImage);
		bigHand.add(myCard);
		
		myCard = new Card(mySuit.SPADES, myType.EIGHT, myImage);
		bigHand.add(myCard);
		
		return bigHand;
	}
	
	public Hand ThreeOfKind() 
	{
        myCard = new Card(mySuit.DIAMONDS, myType.ACE, myImage);
		smallHand.add(myCard);
		
		myCard = new Card(mySuit.CLUBS, myType.ACE, myImage);
		smallHand.add(myCard);
		
		myCard = new Card(mySuit.HEARTS, myType.ACE, myImage);
		smallHand.add(myCard);
		
		myCard = new Card(mySuit.SPADES, myType.FIVE, myImage);
		smallHand.add(myCard);
		
		myCard = new Card(mySuit.SPADES, myType.EIGHT, myImage);
		smallHand.add(myCard);
		
		return smallHand;
	}
	
	/**
	 * A helper function that build a a completely new deck in order.
	 * 
	 * @Dominic
	 */
	
	private Vector<CardType> myCardType;
	private Vector<CardSuit> myCardSuit;
	private Vector<Card> myCards;
	
	public void orderingCard()
	{
		myCardSuit = new Vector<CardSuit>(4);
		myCardType = new Vector<CardType>(13);
		
		myCardSuit.add(mySuit.CLUBS);
		myCardSuit.add(mySuit.DIAMONDS);
		myCardSuit.add(mySuit.HEARTS);
		myCardSuit.add(mySuit.SPADES);
		
		myCardType.add(myType.ACE);
		myCardType.add(myType.TWO);
		myCardType.add(myType.THREE);
		myCardType.add(myType.FOUR);
		myCardType.add(myType.FIVE);
		myCardType.add(myType.SIX);
		myCardType.add(myType.SEVEN);
		myCardType.add(myType.EIGHT);
		myCardType.add(myType.NINE);
		myCardType.add(myType.TEN);
		myCardType.add(myType.JACK);
		myCardType.add(myType.QUEEN);
		myCardType.add(myType.KING);
	}
	
	public Vector<Card> newDeck()
	{
		orderingCard();
		
		myCards = new Vector<Card>(52);
		
		for(int j = 0; j < myCardSuit.capacity(); j++)
		{
			for(int x = 0; x < myCardType.capacity(); x++)
			{
				myCard = new Card(myCardSuit.elementAt(j), myCardType.elementAt(x), myImage);
				myCards.add(myCard);
				
			}
		}
		return myCards;
	}
}
