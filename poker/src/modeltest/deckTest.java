package modeltest;

import static org.junit.Assert.*;

import java.awt.Image;
import java.util.Random;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardSuit;
import model.CardType;
import model.Deck;

public class deckTest 
{
	private Deck myDeck;
	private Card myCard;
	private CardSuit mySuit;
	private CardType myType;
	private Image myImage;
	private Vector<Card> myCards;
	private int cardsinDeck = 52;
	private Vector<CardSuit> myCardSuit;
	private Vector<CardType> myCardType;
	int drawNum;
	
	@Before
	public void setup()
	{
		myDeck = new Deck();	
	}
	
	@Test
	public void testConstructDeck() 
	{
		newDeck();
		
		assertTrue("Should be a completely new deck", myDeck.constructDeck());
	}

	@Test
	public void testCDraw1() 
	{
		newDeck();
		
		drawHelper();
		assertTrue("Should draw " + drawNum, myDeck.draw() == myCards.elementAt(drawNum));
	}
	
	@Test
	public void testCDraw2() 
	{
		newDeck();
		
		drawHelper();
		assertTrue("Should draw " + drawNum, myDeck.draw() == myCards.elementAt(drawNum));
		
		drawHelper();
		assertTrue("Should draw " + drawNum, myDeck.draw() == myCards.elementAt(drawNum));
	}
	
	@Test
	public void testShuffle()
	{
		newDeck();
		assertFalse("Should be new deck", myDeck.shuffle());
		
		shuffleDeck();
		assertTrue("Should be shuffled deck", myDeck.shuffle());
	}
	
	/**
	 * A helper function that build a a completely new deck in order.
	 * 
	 * @Dominic
	 */
	
	public void orderingCard()
	{
		myCardSuit = new Vector<CardSuit>(4);
		myCardType = new Vector<CardType>(13);
		
		myCardSuit.add(mySuit.CLUBS);
		myCardSuit.add(mySuit.DIAMONDS);
		myCardSuit.add(mySuit.HEARTS);
		myCardSuit.add(mySuit.SPADES);
		
		myCardType.add(myType.ACE);
		myCardType.add(myType.TWO);
		myCardType.add(myType.THREE);
		myCardType.add(myType.FOUR);
		myCardType.add(myType.FIVE);
		myCardType.add(myType.SIX);
		myCardType.add(myType.SEVEN);
		myCardType.add(myType.EIGHT);
		myCardType.add(myType.NINE);
		myCardType.add(myType.TEN);
		myCardType.add(myType.JACK);
		myCardType.add(myType.QUEEN);
		myCardType.add(myType.KING);
	}
	
	public Vector<Card> newDeck()
	{
		orderingCard();
		
		myCards = new Vector<Card>(52);
		
		for(int j = 0; j < myCardSuit.capacity(); j++)
		{
			for(int x = 0; x < myCardType.capacity(); x++)
			{
				myCard = new Card(myCardSuit.elementAt(j), myCardType.elementAt(x), myImage);
				myCards.add(myCard);
				
			}
		}
		return myCards;
	}
	
	/**
	 * A helper function that randomly select a card to draw.
	 * 
	 * @Dominic
	 */
	
	public int drawHelper()
	{	
		Random draw = new Random(cardsinDeck);
		drawNum = draw.nextInt((cardsinDeck - 0) + 1) + 0;
		return drawNum;
	}
	
	/**
	 * A helper function that shuffle deck.
	 * 
	 * @Dominic
	 */
	
	public Vector<Card> shuffleDeck()
	{

		newDeck();

		Vector<Card> shuffle = new Vector(52);

		for (int i = 0; i < shuffle.capacity(); i++) 
		{
			int rand = (int) (Math.random() * (i + 1));

			shuffle.add(myCards.elementAt(rand));
			
			myCards.equals(shuffle);
		}
		
		return myCards;
	}
}

